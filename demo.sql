/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 50730
 Source Host           : localhost:3306
 Source Schema         : demo

 Target Server Type    : MySQL
 Target Server Version : 50730
 File Encoding         : 65001

 Date: 07/06/2024 09:48:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for class
-- ----------------------------
DROP TABLE IF EXISTS `class`;
CREATE TABLE `class`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '班级名称',
  `class_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '班级编号',
  `is_del` int(255) NULL DEFAULT 0 COMMENT '1删除  0未删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of class
-- ----------------------------
INSERT INTO `class` VALUES (1, '软工1班', 'A001', 0);
INSERT INTO `class` VALUES (2, '软工2班', 'A002', 0);
INSERT INTO `class` VALUES (3, '软工3班', 'A003', 0);

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `class_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `age` int(11) NULL DEFAULT NULL,
  `sex` int(11) NULL DEFAULT NULL COMMENT '1男  2女',
  `is_del` int(11) NULL DEFAULT 0 COMMENT '1已删除  0未删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES (1, '李四', 'A001', 18, 1, 0);
INSERT INTO `student` VALUES (2, '王五', 'A001', 23, 1, 0);
INSERT INTO `student` VALUES (3, '老六', 'A001', 20, 1, 0);
INSERT INTO `student` VALUES (4, '老八', 'A001', 24, 2, 0);
INSERT INTO `student` VALUES (6, '老七', 'A002', 22, 2, 0);
INSERT INTO `student` VALUES (7, '老久', 'A002', 22, 2, 0);
INSERT INTO `student` VALUES (8, '嘟嘟', 'A002', 22, 2, 0);

SET FOREIGN_KEY_CHECKS = 1;
