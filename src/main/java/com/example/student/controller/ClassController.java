package com.example.student.controller;


import com.example.student.common.Result;
import com.example.student.pojo.ClassDO;
import com.example.student.service.ClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/class")
public class ClassController {


    @Autowired
    private ClassService classService;


    /**
     * 新增班级
     * @param classDO
     * @return
     */
    @PostMapping("/add")
    public Result add(@RequestBody ClassDO classDO){
        if (ObjectUtils.isEmpty(classDO)){
            return Result.result("204","缺少新增参数！");
        }
        int rows = classService.add(classDO);
        return rows>0?Result.result("200","新增班级成功！"):Result.result("204","新增班级失败！");
    }


    /**
     * 获取所有班级
     * @return
     */
    @GetMapping("/getAll")
    public Result getAll(){
        List<ClassDO> classList = classService.list();
        return Result.result("200","获取所有班级成功！",classList);
    }


    /**
     * 根据id删除班级
     * @param id
     * @return
     */
    @DeleteMapping("/deleteById/{id}")
    public Result deleteById(@PathVariable("id")Long id){
        if (ObjectUtils.isEmpty(id)){
            return Result.result("204","缺少班级id参数！");
        }
        int rows = classService.deleteById(id);
        return rows > 0?Result.result("200","删除班级成功"):Result.result("204","删除班级失败！");
    }


    /**
     * 修改班级参数错误
     * @param classDO
     * @return
     */
    @PostMapping("/update")
    public Result updateById(@RequestBody ClassDO classDO){
        if (ObjectUtils.isEmpty(classDO)){
            return Result.result("204","修改参数错误！");
        }
        boolean update = classService.updateById(classDO);
        return update?Result.result("200","修改班级成功！"):Result.result("204","修改班级失败！");
    }


}
