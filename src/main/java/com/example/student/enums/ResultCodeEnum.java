package com.example.student.enums;


import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
public enum ResultCodeEnum {

    SUCCESS("200","请求成功!");

    public String code;

    public String msg;

}
