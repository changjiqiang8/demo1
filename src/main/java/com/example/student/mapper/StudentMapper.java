package com.example.student.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.student.pojo.StudentDO;
import com.example.student.pojo.vo.StudentVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface StudentMapper extends BaseMapper<StudentDO> {


    @Select("select s.id as id,s.name as name,s.age as age,s.sex as sex,s.class_number as classNumber,c.class_name as className " +
            "from student s " +
            "left join class c on s.class_number = c.class_number " +
            "where s.class_number = #{classNumber} and s.is_del = 0")
    List<StudentVO> getStudentsByClassNumber(@Param("classNumber") String classNumber);
}
