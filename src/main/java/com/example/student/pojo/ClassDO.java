package com.example.student.pojo;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("class")
public class ClassDO {


    @TableId(type = IdType.AUTO)
    private Long id;

    private String className;

    private String classNumber;

    private Integer isDel;


}
