package com.example.student.pojo.vo;

import lombok.Data;


@Data
public class StudentVO {

    private Long id;

    private String name;

    private String classNumber;

    private Integer age;

    private Integer sex;

    private Integer isDel;

    private String className;

}
