package com.example.student.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.student.pojo.ClassDO;


public interface ClassService extends IService<ClassDO> {
    int add(ClassDO classDO);

    int deleteById(Long id);
}
