package com.example.student.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.student.pojo.StudentDO;
import com.example.student.pojo.vo.StudentVO;

import java.util.List;


public interface StudentService extends IService<StudentDO> {

    int deleteById(Long id);

    int add(StudentDO studentDO);

    List<StudentVO> getStudentsByClassNumber(String classNumber);
}
