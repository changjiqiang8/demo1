package com.example.student.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.student.mapper.ClassMapper;
import com.example.student.pojo.ClassDO;
import com.example.student.service.ClassService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ClassServiceImpl extends ServiceImpl<ClassMapper, ClassDO> implements ClassService {
    @Override
    @Transactional
    public int add(ClassDO classDO) {
        int rows = this.baseMapper.insert(classDO);
        return rows;
    }

    @Override
    @Transactional
    public int deleteById(Long id) {
        int rows = deleteById(id);
        return rows;
    }

}
