package com.example.student.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.student.mapper.StudentMapper;
import com.example.student.pojo.StudentDO;
import com.example.student.pojo.vo.StudentVO;
import com.example.student.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, StudentDO> implements StudentService {

    @Autowired
    private StudentMapper studentMapper;

    @Override
    @Transactional
    public int deleteById(Long id) {
        int rows = studentMapper.deleteById(id);
        return rows;
    }

    @Override
    @Transactional
    public int add(StudentDO studentDO) {
        int insert = studentMapper.insert(studentDO);
        return insert;
    }

    @Override
    public List<StudentVO> getStudentsByClassNumber(String classNumber) {
        List<StudentVO> studentVOList = studentMapper.getStudentsByClassNumber(classNumber);
        return studentVOList;
    }
}
